## Basic color guessing game 

The goal of the game is to guess which of the tiles on the board contains a higher red, green or blue value (RGB) based on what the user has chosen to guess. 

## Instructions
- To begin, enter your name (minimum 5 characters, with only letters, numbers and spaces).
- Then enter the size of the board, which will generate a game board with the same number of rows and columns (between 3 and 7). 
- Next, choose a color to guess
- Finally a difficulty, which makes the color more and more obscure and thus makes it harder to guess the red, green or blue values. 

Once the board is generated, click on the tiles that you think contain the highest 
red, green or blue values, depending on what you chose above. Once you've made your guesses, 
simply submit your guesses and your score will be calculated based on the number correct 
answers, the number of tiles your guessed correctly, the size of the board and the 
diffulty. 

The score board shows the top 10 scores in descending order. Simply click the score
header to display them in ascending order, and you can toggle this feature back and forth.

Your scores are saved between different browser sessions. If you wish to wipe them out, 
simply click on the clear button in the high score area, and all your saved scores will
be wiped out, and you will start from scratch. 

The round restarts, and you will simply repeat the steps above. 

Enjoy the game !

### Visit our game on our website
sonic.dawsoncollege.qc.ca/~2233420/320/web2-project
