"use strict";

document.addEventListener("DOMContentLoaded", function (e) {
    let descending = true;
    let scores;

    if (localStorage.getItem("scores")) {
        displayScoreDescending();
        scores = fetchDataFromLocalStorage();
    } else {
        scores = [];
    }

    /**
     * On form submission, we disable the form, 
     * empty out the board of previous games,
     * create a new board and set the appropriate colors 
     */
    const form = document.forms[0];
    form.addEventListener("submit", function(e) {
        e.preventDefault();
        disableForm(e.target);
        emptyBoard();
        setBoard(e.target.size.value);
        setColors(Number(e.target.difficulty.value));
        addCheats();
        targetMessage();
    });

    /**
     * Toggle tile selection and button submission on
     * the board
     */
    const board = document.getElementById("board");
    board.addEventListener("click", function(e) {
        let el = e.target;
        if (el.tagName === "SECTION") {
            return;
        }
        if (e.target.tagName === "P") {
            el = e.target.parentElement;
        }
        toggleTileSelection(el);
        targetMessage();
        if (e.target.id === "guess-button") {
            targetMessageRemove();
        }
    });

    /**
     * this event listener retrieves all necessary information for the determineScores 
     * method, calculates high score, and executes necessary 
     * functions to append and store the results
     */
    const guessButton = document.getElementById("guess-button");
    guessButton.addEventListener("click", function(e) {
        e.preventDefault();
        displayPercentage();
        const size = document.getElementById("size").value;
        const difficulty = document.getElementById("difficulty").value;
        const name = document.getElementById("name").value;
        const numberOfcorrectGuesses = determineNumberOfValidSelections();
        const numberOfSelectedTiles = Array
            .from(document.querySelectorAll(".tile"))
            .filter(tile => tile.hasAttribute("data-selected"))
            .length;
        const score = determineScore(numberOfcorrectGuesses, 
            numberOfSelectedTiles, size, difficulty);
        scores.push({name: name, score: score});
        saveNamesAndScoresToLocalStorage(scores);
        emptyHighScoreDOM();
        displayScoreDescending();
        enableForm();
        emptyBoard();
    });

    /**
     * Update color of form button depending on 
     * color input value
     */
    document.getElementById("color").addEventListener("change", function (e) {
        changeButton(e.target.value);
    });

    /**
     * Toggle ascending/descending order of scores
     */  
    const scoreHeader = document.getElementById("score-header");
    scoreHeader.addEventListener("click", function(e) {
        if (descending) {
            emptyHighScoreDOM();
            displayScoreAscending();
            descending = false;
        } else {
            emptyHighScoreDOM();
            displayScoreDescending();
            descending = true;
        }
    });

    /**
     * Empties high score
     */
    const clearScoreButton = document.getElementById("clear");
    clearScoreButton.addEventListener("click", function(e) {
        emptyHighScore();
        scores = [];
    });

    /**
     * Cheat mode
     */
    document.addEventListener("keydown", function (e) {
        if (e.shiftKey && e.key === "C" && e.target.tagName !== "INPUT" ) {
            toggleCheat(e);
        }
    });

});